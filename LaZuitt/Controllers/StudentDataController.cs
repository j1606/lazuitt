using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LaZuitt.Models;

namespace LaZuitt.Controllers
{
    [Route("api/StudentData")]
    [ApiController]
    public class StudentDataController : ControllerBase
    {
        private readonly StudentInfoContext _context;

        public StudentDataController(StudentInfoContext context)
        {
            _context = context;
        }

        // GET: api/StudentData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentInfoDTO>>> GetStudentInfos()
        {
          //if (_context.StudentInfos == null)
          //{
          //    return NotFound();
          //}
          //  return await _context.StudentInfos.ToListAsync();

          return await _context.StudentInfos
                .Select(x => InfoToDTO(x))
                .ToListAsync();
        }

        // GET: api/StudentData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentInfoDTO>> GetStudentInfo(long id)
        {
          //if (_context.StudentInfos == null)
          //{
          //    return NotFound();
          //}
            var studentInfo = await _context.StudentInfos.FindAsync(id);

            if (studentInfo == null)
            {
                return NotFound();
            }

            return InfoToDTO(studentInfo);
        }

        // PUT: api/StudentData/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStudentInfo(long id, StudentInfoDTO studentInfoDTO)
        {
            if (id != studentInfoDTO.Id)
            {
                return BadRequest();
            }

            var studentInfo = await _context.StudentInfos.FindAsync(id);
            if (studentInfo == null)
            {
                return NotFound();
            }

            studentInfo.Name = studentInfoDTO.Name;
            studentInfo.Course = studentInfoDTO.Course;
            studentInfo.IsEnrolled = studentInfoDTO.IsEnrolled;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!StudentInfoExists(id))
            {
                return NotFound();
            }

            //_context.Entry(studentInfo).State = EntityState.Modified;

            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!StudentInfoExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return NoContent();
        }

        // POST: api/StudentData
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<StudentInfoDTO>> CreateStudentInfo(StudentInfoDTO studentInfoDTO)
        {
            var studentInfo = new StudentInfo
            {
                Name = studentInfoDTO.Name,
                Course = studentInfoDTO.Course,
                IsEnrolled = studentInfoDTO.IsEnrolled,
                
            };

            _context.StudentInfos.Add(studentInfo);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetStudentInfo),
                new { id = studentInfo.Id },
                InfoToDTO(studentInfo));
          //  if (_context.StudentInfos == null)
          //{
          //    return Problem("Entity set 'StudentInfoContext.StudentInfos'  is null.");
          //}
          //  _context.StudentInfos.Add(studentInfo);
          //  await _context.SaveChangesAsync();

          //  //return CreatedAtAction("GetStudentInfo", new { id = studentInfo.Id }, studentInfo);
          //  return CreatedAtAction(nameof(GetStudentInfo), new { id = studentInfo.Id }, studentInfo);
        }

        // DELETE: api/StudentData/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudentInfo(long id)
        {
            //if (_context.StudentInfos == null)
            //{
            //    return NotFound();
            //}
            //var studentInfo = await _context.StudentInfos.FindAsync(id);
            //if (studentInfo == null)
            //{
            //    return NotFound();
            //}

            //_context.StudentInfos.Remove(studentInfo);
            //await _context.SaveChangesAsync();

            //return NoContent();

            var todoItem = await _context.StudentInfos.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            _context.StudentInfos.Remove(todoItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StudentInfoExists(long id)
        {
            return _context.StudentInfos.Any(e => e.Id == id);
        }

        private static StudentInfoDTO InfoToDTO(StudentInfo studentInfo) =>
           new StudentInfoDTO
           {
               Id = studentInfo.Id,
               Name = studentInfo.Name,
               Course = studentInfo.Course,
               IsEnrolled = studentInfo.IsEnrolled
           };
    }
}
