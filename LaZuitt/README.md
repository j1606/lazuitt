﻿# ASP.NET Web API: Models and Controllers

## Creating your template to start up.

    1. File -> New Project
    2. Select APi under web and console.
    3. Name your project then create.
    4. Right click your project then select **Open in terminal**.
    5. Run this code to add this package to your project:

        dotnet add package Microsoft.EntityFrameworkCore.InMemory

    6. Test and run your project.
    7. Lastly, go to **_Properties\launchSettings.json_** and change the **launchUrl** calue to your project url.


## Design of the app.

![image](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-web-api/_static/architecture.png?view=aspnetcore-6.0)


## Adding Model class to our project:

    A model is a set of classes that represent the data that the app manages.
    Right click your project then add a folder and name it: Models. This will serve your container for all your models in your project.
    Inside that folder create a class model with the following code below:

    ```
    namespace <project_name>.Models
    {
        public class <model_name>
        {
            // you can change this code based your project requirements.
            public long Id { get; set; }
            public string? Name { get; set; }
            public string? Course { get; set; }
            public bool IsEnrolled { get; set; }
        }
    }
    ```

## Adding a Databse Context

    The database context is the main class that coordinates Entity Framework functionality for a data model.
    This class is created by deriving from the Microsoft.EntityFrameworkCore.DbContext class.
    Inside your Models folder, create a class for your Database Context and type in the following code below:

    ```
    using Microsoft.EntityFrameworkCore;
    using System.Diagnostics.CodeAnalysis;

    namespace <project_name>.Models
    {
        public class <db_context_name> : DbContext
        {
            public db_context_name(DbContextOptions<db_context_name> options)
                : base(options)
            {
            }

            public DbSet< <model_name> > <SampleItems/plural_form_of_your model_name> { get; set; } = null!;
        }
    }
    ```.

## Register your Database Context

    In ASP.NET Core, services such as the DB context must be registered with the dependency injection (DI) container.
    The container provides the service to controllers.
    Update the **_Program.cs_** with the following code:

    ```
    using Microsoft.EntityFrameworkCore;
    using LaZuitt.Models;


    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.

    builder.Services.AddControllers();
    builder.Services.AddDbContext<StudentInfoContext>(opt =>
        opt.UseInMemoryDatabase("StudentCredentials"));


    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    //builder.Services.AddEndpointsApiExplorer();
    //builder.Services.AddSwaggerGen();

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (builder.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        //app.UseSwagger();
        //app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
    ```

## Scaffold a Controller

    This will serve a quick way to add code that interacts with our data models.

    The generated code:

    * Marks the class with the [ApiController] attribute. This attribute indicates that the controller responds to web API requests.
    * Uses DI to inject the database context (db_context_name) into the controller. The database context is used in each of the CRUD methods in the controller.

**_Note:_**

    The ASP.NET Core templates for:

    * Controllers with views include [action] in the route template.
    * API controllers don't include [action] in the route template.

    When the [action] token isn't in the route template, the action name is excluded from the route. That is, the action's associated method name isn't used in the matching route.

## Update [HttpPost]

    Update the return statement in the <HttpPost_section> to use the nameof operator:

    This method gets the value of the to-do item from the body of the HTTP request.

    ```
    [HttpPost]
    public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
    {
 
         if (_context.StudentInfos == null)
         {
             return Problem("Entity set 'StudentInfoContext.StudentInfos'  is null.");
         }
           _context.StudentInfos.Add(studentInfo);
           await _context.SaveChangesAsync();

         //return CreatedAtAction("GetStudentInfo", new { id = studentInfo.Id }, studentInfo);
         return CreatedAtAction(nameof(GetStudentInfo), new { id = studentInfo.Id }, studentInfo);
    }
    ```

    The C# **_nameof_** keyword is used to avoid hard-coding the action name in the **_CreatedAtAction_** call.

    The CreatedAtAction method:

        * Returns an HTTP 201 status code if successful. HTTP 201 is the standard response for an HTTP POST method that creates a new resource on the server.
        * Adds a Location header to the response. The Location header specifies the URI of the newly created list item.



# ASP.NET Web API : CRUD Operations and API Routing

## Install http-repl

    It is used for making HTTP requests to test ASP.NET Core web APIs (and non-ASP.NET Core web APIs) and view their results.
    In your terminal, copy paste this code to install http-repl:

    ```
    dotnet tool install -g Microsoft.dotnet-httprepl
    ```

 Inside the controller.cs file after you scaffold a controller it created Http methods already that corresponds to CRUD (Create, Read, Update, Delete).

 Before testing out CRUD, you need to run your url together with httprelp keyword in your terminal.

 ```
 httprepl https://localhost:<port>/<your_defined_url_route>
 ```

### Create

    This will insert value to our model.

    ```
    post -h Content-Type=application/json -c "{"key_variable":"value","key_variable":"value"...}"
    ```

### Read

    This will retrieve the value inserted in our model adn return values in your terminal.
    Use the keyword connect to specify what route you are going to.

    ``` 
    connect https://localhost:<port>/<your_defined_url_route>/{id}
    get
    ```

### Update

    With this, you can update values you inserted using post method.

    ```
    connect https://localhost:<port>/<your_defined_url_route>/{id}
    put -h Content-Type=application/json -c "{"{id}": <value>,"key_variable":"value","key_variable":"value"...}"
    ```

### Delete

    This will a data.

    ```
    connect https://localhost:<port>/<your_defined_url_route>/{id}
    delete
    ```

## Data Transfer Object

    It lets you to prevent over-posting data that should not be on user' screen or private information.

**_A DTO may be used to:_**
* Hide properties that clients are not supposed to view.
* Omit some properties in order to reduce payload size.
* Flatten object graphs that contain nested objects. Flattened object graphs can be more convenient for clients.

To demonstrate DTO approach, update your model class file adding the following line of code:

```
namespace <Project_name>.Models
{
    public class <Model_name>
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Course { get; set; }
        public bool IsEnrolled { get; set; }
        public string? Secret { get; set; }
    }
}
```


Then, create a class inside Models folder with the follwing code block:

```
        namespace <Project_name>.Models
        {
            public class <DTO_name>
            {
                public long Id { get; set; }
                public string? Name { get; set; }
                public string? Course { get; set; }
                public bool IsEnrolled { get; set; }
                
            }
        }
```

Lastly update your controller file with the following block of codes:

**_Note:_** variable names, keywords and classes are based on the sample project.

```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LaZuitt.Models;

namespace LaZuitt.Controllers
{
    [Route("api/StudentData")]
    [ApiController]
    public class StudentDataController : ControllerBase
    {
        private readonly StudentInfoContext _context;

        public StudentDataController(StudentInfoContext context)
        {
            _context = context;
        }

        // GET: api/StudentData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentInfoDTO>>> GetStudentInfos()
        {
          return await _context.StudentInfos
                .Select(x => InfoToDTO(x))
                .ToListAsync();
        }

        // GET: api/StudentData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentInfoDTO>> GetStudentInfo(long id)
        {
            var studentInfo = await _context.StudentInfos.FindAsync(id);

            if (studentInfo == null)
            {
                return NotFound();
            }

            return InfoToDTO(studentInfo);
        }

        // PUT: api/StudentData/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStudentInfo(long id, StudentInfoDTO studentInfoDTO)
        {
            if (id != studentInfoDTO.Id)
            {
                return BadRequest();
            }

            var studentInfo = await _context.StudentInfos.FindAsync(id);
            if (studentInfo == null)
            {
                return NotFound();
            }

            studentInfo.Name = studentInfoDTO.Name;
            studentInfo.IsEnrolled = studentInfoDTO.IsEnrolled;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!StudentInfoExists(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/StudentData
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<StudentInfoDTO>> CreateStudentInfo(StudentInfoDTO studentInfoDTO)
        {
            var studentInfo = new StudentInfo
            {
                IsEnrolled = studentInfoDTO.IsEnrolled,
                Name = studentInfoDTO.Name
            };

            _context.StudentInfos.Add(studentInfo);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetStudentInfo),
                new { id = studentInfo.Id },
                InfoToDTO(studentInfo));
        }

        // DELETE: api/StudentData/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudentInfo(long id)
        {
            var todoItem = await _context.StudentInfos.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            _context.StudentInfos.Remove(todoItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StudentInfoExists(long id)
        {
            return _context.StudentInfos.Any(e => e.Id == id);
        }

        private static StudentInfoDTO InfoToDTO(StudentInfo studentInfo) =>
           new StudentInfoDTO
           {
               Id = studentInfo.Id,
               Name = studentInfo.Name,
               Course = studentInfo.Course,
               IsEnrolled = studentInfo.IsEnrolled
           };
    }
}

```



**_Additional Note:_**

*   Check if the ssl is trusted by your machine.
    If not trusted use this code:
    ```
    dotnet dev-certs https --trust
    ```

*   If the httprepl is not working then make sure that .dotnet/tools/ added to your file.
    To add dotnet tools to your path create a .zshrc file inside home directory.
    Then inside that file add the path sample:

    ```
    export PATH="$PATH:/Users/<home_name>/.dotnet/tools"
    ```